package mqtt;

import android.content.Context;
import android.util.Log;
import android.widget.Button;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

public class SensorGenerico {

    private final String device;
    private String ipGateway;
    private Button visualValor;
    private MqttAndroidClient mqttAndroidClient;
    private String serverUri;

    public SensorGenerico(Context context, String ipGateway, final String device){


        serverUri = "tcp://"+ ipGateway +":1883";
        this.device = device;

        mqttAndroidClient = new MqttAndroidClient(context, serverUri, "sensorAndroidPassos");

        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.w("mqtt", s);
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                //Log.w("Mqtt", mqttMessage.toString());
                //Log.w("mqtt received call 1", mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(false);
        mqttConnectOptions.setCleanSession(true);
        // try {
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {

                Log.w("mqtt received callback 2", message.toString());

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

/*
        } catch (MqttException m){
            Log.w("Mqtt", m.getMessage());
        }*/
    }

    public void subescrever(){

        try {
            IMqttToken token = mqttAndroidClient.connect();

            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {

                    try {
                        mqttAndroidClient.subscribe(device, 2);
                        //Log.i("mqtt", "Message published");

                    } catch (MqttPersistenceException e) {
                        e.printStackTrace();

                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

                }
            });

        } catch (MqttException m){
            m.printStackTrace();
        }

    }



}
