package com.example.necio.passos;

public interface StepListener {

    public void step(long timeNs);
}
